package org.fboyer.mastermind.service.impl;


import org.apache.commons.lang3.StringUtils;

import org.fboyer.mastermind.entity.GameEntity;
import org.fboyer.mastermind.exception.EngineException;
import org.fboyer.mastermind.model.enumeration.ErrorTypeEnum;
import org.fboyer.mastermind.repository.GameRepository;
import org.fboyer.mastermind.service.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;

/**
 * Game Service implementation.
 *
 * @author fboyer on 26/09/2018
 */
@Service
public class GameServiceImpl implements GameService {

    @Autowired
    private GameRepository repository;

    private static final Integer GAME_PATTERN_SIZE = 4;

    @Transactional
    @Override
    public GameEntity get(final Long gameId) {
        if (gameId == null) {
            throw new EngineException(ErrorTypeEnum.MISSING_PARAMETER, "Game id cannot be null.");
        }
        final GameEntity gameEntity = this.repository.findById(gameId).get();
        if (gameEntity == null) {
            throw new EngineException(ErrorTypeEnum.GAME_NOT_FOUND, "Game with id " + gameId + " was not found.");
        }
        return gameEntity;
    }

    @Transactional
    @Override
    public GameEntity create(final GameEntity gameEntity) {
        this.createPreconditions(gameEntity);
        return this.repository.save(gameEntity);
    }

    private void createPreconditions (final GameEntity gameEntity) {
        if (gameEntity == null){
            throw new EngineException(ErrorTypeEnum.MISSING_PARAMETER, "Game cannot be null.");
        } else if (StringUtils.isBlank(gameEntity.getCodeMakerName())){
            throw new EngineException(ErrorTypeEnum.MISSING_PARAMETER, "Game code maker name cannot be null.");
        } else if (StringUtils.isBlank(gameEntity.getPattern())) {
            throw new EngineException(ErrorTypeEnum.MISSING_PARAMETER, "Game pattern cannot be null.");
        }
        final List<String> patern = Arrays.asList(gameEntity.getPattern().split(","));
        if (patern.size()!= GameServiceImpl.GAME_PATTERN_SIZE){
            throw new EngineException(ErrorTypeEnum.GAME_NOT_VALID, "Game pattern is invalid.");
        }
    }

    @Transactional
    @Override
    public void delete(final Long gameId) {
        try {
            this.get(gameId);
            this.repository.deleteById(gameId);
        } catch (final EngineException engineException) {
            if (engineException.getErrorType() != ErrorTypeEnum.GAME_NOT_FOUND) {
                throw engineException;
            }
        }
    }

    @Transactional
    @Override
    public List<GameEntity> list() {
        return this.repository.findAll();
    }

    @Transactional
    @Override
    public List<GameEntity> listFilterByName(final String name) {
        final List<GameEntity> response;
        final String cleanName = StringUtils.removeStart(name, " ");
        if (StringUtils.isBlank(cleanName)) {
            throw new EngineException(ErrorTypeEnum.INVALID_PARAMETER, "Name '" + cleanName + "' cannot be null or empty");
        } else {
            response = this.repository.findAllByCodeMakerName(cleanName);
        }
        return response;
    }

    @Transactional
    @Override
    public GameEntity getLastGameActive() {
        return this.repository.findTop1ByOrderByCreatedAt();
    }
}
