package org.fboyer.mastermind.service.impl;



import org.fboyer.mastermind.entity.GameEntity;
import org.fboyer.mastermind.entity.GameResponseEntity;
import org.fboyer.mastermind.exception.EngineException;
import org.fboyer.mastermind.model.enumeration.ErrorTypeEnum;
import org.fboyer.mastermind.service.GameResponseService;
import org.fboyer.mastermind.service.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Game Response Service implementation.
 *
 * @author fboyer on 26/09/2018
 */
@Service
public class GameResponseServiceImpl implements GameResponseService {

    @Autowired
    private GameService gameService;

    private final HashMap<Integer, String> patternMap = new HashMap<>();
    private final HashMap<Integer, String> patternEntryMap = new HashMap<>();

    @Transactional
    @Override
    public GameResponseEntity process(final List<String> patternEntry) {
        final GameEntity gameEntity = this.gameService.getLastGameActive();
        if (gameEntity == null) {
            throw new EngineException(ErrorTypeEnum.GAME_NOT_FOUND, "No game found");
        }
        final List<String> gamePattern = Arrays.asList(gameEntity.getPattern().split(","));
        this.initializeMaps(gamePattern,patternEntry);

        final Integer blacks = this.getBlacks(gamePattern, patternEntry);
        final Integer oranges = this.getOranges();

        final GameResponseEntity responseEntity = new GameResponseEntity();
        responseEntity.setBlacks(blacks);
        responseEntity.setOrange(oranges);

        return responseEntity;
    }

    private void initializeMaps (final List<String> gamePattern, final List<String> patternEntry) {
        for (int i=0;i< gamePattern.size();i++){
            this.patternMap.put(i,gamePattern.get(i));
        }
        for (int i=0;i< patternEntry.size();i++){
            this.patternEntryMap.put(i,gamePattern.get(i));
        }
    }

    private Integer getBlacks (final List<String> gamePattern, final List<String> patternEntry){
        Integer res = 0;
        for (int i=0;i< gamePattern.size();i++){
            final String colorGamePattern = gamePattern.get(i).toLowerCase();
            final String colorPatternEntry = patternEntry.get(i).toLowerCase();
            if (colorGamePattern.equals(colorPatternEntry)){
                res ++;
                this.patternMap.remove(i);
                this.patternEntryMap.remove(i);
            }
        }
        return res;
    }

    private Integer getOranges () {
        Integer res = 0;
        final List<String> gamePattern = (List<String>) this.patternMap.values();
        final List<String> gameEntryPattern =  (List<String>) this.patternEntryMap.values();

        for (final String color: gameEntryPattern){
            if (gamePattern.contains(color)){
                res ++;
                gamePattern.remove(color);
            }
        }
        return  res;
    }
}
