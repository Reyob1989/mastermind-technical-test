package org.fboyer.mastermind.mapper;


import org.fboyer.mastermind.entity.SampleEntity;
import org.fboyer.mastermind.model.Sample;
import org.mapstruct.Mapper;

/**
 *
 * @author dsepulveda on 2018-09-27
 */
@Mapper(componentModel = "spring")
public interface SampleMapper {

	Sample toModel(SampleEntity source);

	SampleEntity toEntity(Sample source);

}
