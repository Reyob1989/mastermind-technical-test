package org.fboyer.mastermind.repository;



import org.fboyer.mastermind.entity.GameEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Repository for games on {@link GameEntity} model object
 *
 * @author fboyer on 25/09/2018.
 */
@Repository
public interface GameRepository extends JpaRepository<GameEntity, Long> {

	/**
	 * Get all games in database
	 *
	 * @param name name for filter
	 *
	 * @return  List<GameEntity> list of games of player
	 */
	List<GameEntity> findAllByCodeMakerName(final String name);

	/**
	 * Find last game
	 *
	 * @return GameEntity last game
	 */
	GameEntity findTop1ByOrderByCreatedAt();
}
