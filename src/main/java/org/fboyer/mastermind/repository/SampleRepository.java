package org.fboyer.mastermind.repository;

import org.fboyer.mastermind.entity.SampleEntity;
import org.springframework.data.repository.CrudRepository;

/**
 * <Insert javadoc here>.
 *
 * @author dsepulveda on 2018-09-27
 */
public interface SampleRepository extends CrudRepository<SampleEntity, Long> {

}
