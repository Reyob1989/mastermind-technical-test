package org.fboyer.mastermind.model;

import java.io.Serializable;
import java.util.Date;

/**
 * Base audit model object
 *
 * @author fboyer on 2017-09-12.
 */
public abstract class AbstractAuditableEntity implements Serializable {

	private static final long serialVersionUID = 2102097251869811017L;

	private String createdBy;
	private Date createdAt;
	private Date modifiedAt;

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(final String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(final Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getModifiedAt() {
		return this.modifiedAt;
	}

	public void setModifiedAt(final Date modifiedAt) {
		this.modifiedAt = modifiedAt;
	}

}
