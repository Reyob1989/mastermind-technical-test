package org.fboyer.mastermind.model;

import org.fboyer.mastermind.entity.AuditableEntity;

/**
 * Game class for mapper
 *
 * @author fboyer on 26/09/2018.
 */
public class Game extends AuditableEntity {

    private Long gameId;
    private String codeMakerName;
    private String pattern;

    public Long getGameId() {
        return gameId;
    }

    public void setGameId(Long gameId) {
        this.gameId = gameId;
    }

    public String getCodeMakerName() { return codeMakerName; }

    public void setCodeMakerName(String codeMakerName) { this.codeMakerName = codeMakerName; }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }
}
