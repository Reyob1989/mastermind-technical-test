package org.fboyer.mastermind.model.enumeration;

import java.util.Objects;

/**
 * Channel enumeration.
 *
 * @author fboyer on 2017-09-12.
 */
public enum ColorEnum {
	RED(1, "color.red"),
	BLUE(2, "color.blue"),
	GREEN(3, "color.green"),
	YELLOW(4, "color.yellow"),
	PURPLE(5, "color.purple");

	private static final String ENTITY = "Color";

	private final Integer value;
	private final String literalKey;

	ColorEnum(final Integer value, final String literalKey) {
		this.value = value;
		this.literalKey = literalKey;
	}

	/**
	 * Returns the enum identifier.
	 *
	 * @return The value.
	 */
	public Integer getValue() {
		return this.value;
	}

	/**
	 * Returns the literal message key to be resolved from a resources file.
	 *
	 * @return Message key for looking for in resources property files.
	 */
	public String getLiteralKey() {
		return this.literalKey;
	}

	/**
	 * Resolves a {@link ColorEnum} from its numeric value.
	 *
	 * @param value value to look for.
	 *
	 * @return ChannelEnum.
	 * @throws IllegalArgumentException if the given value is not valid.
	 */
	public static ColorEnum valueOf(final Integer value) {
		for (final ColorEnum instance : ColorEnum.values()) {
			if (Objects.equals(instance.value, value)) {
				return instance;
			}
		}
		throw new IllegalArgumentException("There isn't any " + ColorEnum.ENTITY + " with value '" + value + "'.");
	}
}
